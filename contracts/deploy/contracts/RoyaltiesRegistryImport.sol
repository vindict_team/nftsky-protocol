// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;
pragma abicoder v2;

import "@nftsky/protocol/contracts/royalties/RoyaltiesRegistry.sol";
import "@nftsky/protocol/contracts/royalties/providers/RoyaltiesProviderV2Legacy.sol";
import "@nftsky/protocol/contracts/royalties/providers/RoyaltiesProviderArtBlocks.sol";
