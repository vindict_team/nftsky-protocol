// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

//tokens 721
import "@nftsky/contracts/tokens/ERC721.sol";
import "@nftsky/contracts/tokens/ERC721Minimal.sol";

import "@nftsky/contracts/factories/ERC721FactoryC2.sol";

import "@nftsky/contracts/tokens/ERC721MinimalBeacon.sol";
import "@nftsky/contracts/tokens/ERC721Beacon.sol";

//tokens 1155
import "@nftsky/contracts/tokens/ERC1155.sol";
import "@nftsky/contracts/factories/ERC1155FactoryC2.sol";
import "@nftsky/contracts/tokens/ERC1155Beacon.sol";
