// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

import "@nftsky/contracts/transfer-proxy/lazy-mint/erc721/ERC721LazyMintTransferProxy.sol";
import "@nftsky/contracts/transfer-proxy/lazy-mint/erc1155/ERC1155LazyMintTransferProxy.sol";
import "@nftsky/contracts/transfer-proxy/proxy/TransferProxy.sol";
import "@nftsky/contracts/transfer-proxy/proxy/ERC20TransferProxy.sol";
