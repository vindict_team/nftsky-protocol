//SPDX-License-Identifier: MIT
pragma solidity 0.7.6;
pragma abicoder v2;

import "../../meta-transactions/EIP712MetaTransaction.sol";
import "../ERC1155.sol";

contract ERC1155Meta is ERC1155, EIP712MetaTransaction {

    event CreateERC1155Meta(address owner, string name, string symbol);
    event CreateERC1155UserMeta(address owner, string name, string symbol);

    function __ERC1155UserMeta_init(string memory _name, string memory _symbol, string memory baseURI, string memory contractURI, address[] memory operators, address transferProxy, address lazyTransferProxy) external initializer {
        __ERC1155_init_unchained(_name, _symbol, baseURI, contractURI, transferProxy, lazyTransferProxy);

        for(uint i = 0; i < operators.length; i++) {
            setApprovalForAll(operators[i], true);
        }
        __MetaTransaction_init_unchained("ERC1155UserMeta", "1");
        
        isPrivate = true;

        emit CreateERC1155UserMeta(_msgSender(), _name, _symbol);
    }
    
    function __ERC1155Meta_init(string memory _name, string memory _symbol, string memory baseURI, string memory contractURI, address transferProxy, address lazyTransferProxy) external initializer {
        __ERC1155_init_unchained(_name, _symbol, baseURI, contractURI, transferProxy, lazyTransferProxy);

        __MetaTransaction_init_unchained("ERC1155Meta", "1");

        isPrivate = false;

        emit CreateERC1155Meta(_msgSender(), _name, _symbol);
    }

    function _msgSender() internal view virtual override(ContextUpgradeable, EIP712MetaTransaction) returns (address payable) {
        return super._msgSender();
    }
}
