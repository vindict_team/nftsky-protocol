//SPDX-License-Identifier: MIT
pragma solidity 0.7.6;
pragma abicoder v2;

import "../../meta-transactions/EIP712MetaTransaction.sol";
import "../ERC721Minimal.sol";

contract ERC721Meta is ERC721Minimal, EIP712MetaTransaction {

    event CreateERC721Meta(address owner, string name, string symbol);
    event CreateERC721UserMeta(address owner, string name, string symbol);

    function __ERC721UserMeta_init(string memory _name, string memory _symbol, string memory baseURI, string memory contractURI, address[] memory operators, address transferProxy, address lazyTransferProxy) external initializer {
        __ERC721_init_unchained(_name, _symbol, baseURI, contractURI, transferProxy, lazyTransferProxy);

        for(uint i = 0; i < operators.length; i++) {
            setApprovalForAll(operators[i], true);
        }

        __MetaTransaction_init_unchained("ERC721UserMeta", "1");

        isPrivate = true;

        emit CreateERC721UserMeta(_msgSender(), _name, _symbol);
    }

    function __ERC721Meta_init(string memory _name, string memory _symbol, string memory baseURI, string memory contractURI, address transferProxy, address lazyTransferProxy) external initializer {
        __ERC721_init_unchained(_name, _symbol, baseURI, contractURI, transferProxy, lazyTransferProxy);

        __MetaTransaction_init_unchained("ERC721Meta", "1");

        isPrivate = false;

        emit CreateERC721Meta(_msgSender(), _name, _symbol);
    }

    function _msgSender() internal view virtual override(ContextUpgradeable, EIP712MetaTransaction) returns (address payable) {
        return super._msgSender();
    }
}
