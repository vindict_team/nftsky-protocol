const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const contract = require("@truffle/contract");
const adminJson = require("@openzeppelin/upgrades-core/artifacts/ProxyAdmin.json")
const ProxyAdmin = contract(adminJson)
ProxyAdmin.setProvider(web3.currentProvider)

const { getProxyImplementation } = require("./config.js")

const ERC721 = artifacts.require('ERC721');
const ERC721Beacon = artifacts.require('ERC721Beacon');
const ERC1155 = artifacts.require('ERC1155');
const ERC1155FactoryC2 = artifacts.require('ERC1155FactoryC2');
const ERC1155Beacon = artifacts.require('ERC1155Beacon');
const TransferProxy = artifacts.require('TransferProxy');
const ERC721LazyMintTransferProxy = artifacts.require('ERC721LazyMintTransferProxy');
const ERC1155LazyMintTransferProxy = artifacts.require('ERC1155LazyMintTransferProxy');

module.exports = async function (deployer, network) {
  const transferProxy = (await TransferProxy.deployed()).address;
  const erc721LazyMintTransferProxy = (await ERC721LazyMintTransferProxy.deployed()).address;
  const erc1155LazyMintTransferProxy = (await ERC1155LazyMintTransferProxy.deployed()).address;

  //deploying erc721 proxy
  const erc721Proxy = await deployProxy(ERC721, ["NFT SKY", "NSU", "https://api.nftsky.io/api/v1/", "", transferProxy, erc721LazyMintTransferProxy], { deployer, initializer: '__ERC721_init' });
  console.log("deployed erc721 at", erc721Proxy.address)

  //deploying erc1155 proxy
  const erc1155Proxy = await deployProxy(ERC1155, ["NFT SKY", "NSNU", "https://api.nftsky.io/api/v1/", "", transferProxy, erc1155LazyMintTransferProxy], { deployer, initializer: '__ERC1155_init' });
  console.log("deployed erc1155 at", erc1155Proxy.address)

  //deploying erc712 factory
  //ERC721 implementation
  const erc721 = await getProxyImplementation(ERC721, network, ProxyAdmin)

  //deploying ERC721Beacon
  const beacon721 = await deployer.deploy(ERC721Beacon, erc721, { gas: 1000000 });

  //deploying erc1155 factory
  //ERC1155 implementation
  const erc1155 = await getProxyImplementation(ERC1155, network, ProxyAdmin)

  //deploying ERC1155Beacon
  const beacon1155 = await deployer.deploy(ERC1155Beacon, erc1155, { gas: 1000000 });

  //deploying new factory
  const factory1155 = await deployer.deploy(ERC1155FactoryC2, beacon1155.address, transferProxy, erc1155LazyMintTransferProxy, { gas: 2500000 });
  console.log(`deployed factory1155 at ${factory1155.address}`)

};