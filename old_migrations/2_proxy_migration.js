const ProxyRegistry = artifacts.require("ProxyRegistry");
const AuthenticatedProxy = artifacts.require("AuthenticatedProxy");
const DelegateProxy = artifacts.require("DelegateProxy");
const TokenTransferProxy = artifacts.require("TokenTransferProxy");

module.exports = async (deployer) => {
  const accounts = await web3.eth.getAccounts()
  const owner = accounts[0]
  
  await deployer.deploy(ProxyRegistry).then(async () => {
    await deployer.deploy(AuthenticatedProxy, owner, ProxyRegistry.address);
    await deployer.deploy(DelegateProxy)
    await deployer.deploy(TokenTransferProxy)
  });
};

// Result: 0x5A374dE3B897ad4f95554e980B3205D049729fE6
// AuthenticatedProxy: 
// DelegateProxy: 
// TokenTransferProxy: 
