const ProxyRegistry = artifacts.require("ProxyRegistry");
const Common721 = artifacts.require("Common721");

module.exports = async (deployer, network) => {
  let tokenName = 'NFT SKY 721'
  let tokenCode = 'NSU' // NFT SKY Unique
  if (network == "live") {
    tokenName = 'NFT SKY Token'
    tokenCode = 'NST'
  }
  // const proxy = ProxyRegistry.deployed()
  const proxy = '0x5A374dE3B897ad4f95554e980B3205D049729fE6'
  await deployer.deploy(Common721, tokenName, tokenCode, proxy)
};

// Result: 0x0b6ed8BCb29A3C02f32E05987386EF916C0e3698