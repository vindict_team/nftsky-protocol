const ProxyRegistry = artifacts.require("ProxyRegistry");
const Common1155 = artifacts.require("Common1155");
// const Factory1155 = artifacts.require("Factory1155");
// const Lootbox1155 = artifacts.require("Lootbox1155");

module.exports = async (deployer, network) => {
  let tokenName = 'NFT SKY 1155'
  let tokenCode = 'NSNU' // NFT SKY Non Unique
  if (network == "live") {
    tokenName = 'NFT SKY Collectible'
    tokenCode = 'NSC'
  }
  console.log(ProxyRegistry.isDeployed())
  console.log(ProxyRegistry.hasNetwork())
  console.log('address:' + ProxyRegistry.address)
  console.log('newtork:' + ProxyRegistry.newtork)
  console.log('newtorks:' + ProxyRegistry.newtorks)
  const proxy = await ProxyRegistry.deployed()
  // const proxy = '0x5A374dE3B897ad4f95554e980B3205D049729fE6'
  await deployer.deploy(Common1155, tokenName, tokenCode, proxy.address).then(async () => {
    // await deployer.deploy(Factory1155, proxy, Common1155.address)
    // await deployer.deploy(Lootbox1155, proxy, Common1155.address)
  })
};

// Result: 0xe8f81506715eD8E43724b96a1A643bF6Ac879b0f
// Factory: 0x36CB8d7F8F405ba0d66b1AA09a638Bd1Cf533D3E
// LootBox: 0x69035cb8635718b01cC8A95B4E1693ebC12Bcb97
