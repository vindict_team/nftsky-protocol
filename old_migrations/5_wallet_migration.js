const Wallet = artifacts.require("Wallet");

module.exports = async (deployer, network) => {
  const accounts = await web3.eth.getAccounts()
  const owner = accounts[0]
  let ETHAddress = ''
  if (network == "live") {
    ETHAddress = ''
  }
  await deployer.deploy(Wallet)

  const walletContact = await Wallet.deployed()
  await walletContact.initialize(owner)
};

// Result: 0x9754D2Ad494482088bB2c5Bf9043f57170F791F2
